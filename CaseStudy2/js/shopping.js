let data = [];
let data_cast = [];
let data_order = [];

let FoodInfo = function (food_name, food_price) {
  this.foodName = food_name;
  this.foodPrice = food_price;
};

//Test Onload hiển thị hình từ LocalStorage

let FoodList = function (food_name, food_price, food_image) {
  this.foodNameTest = food_name;
  this.foodPriceTest = food_price;
  this.foodImageTest = food_image;
};

function sliceIntoChunks(arr, chunkSize) {
  const res = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    res.push(chunk);
  }
  return res;
}
function show() {
  var data = localStorage.getItem("data");
  data = JSON.parse(data);
  let new_data = sliceIntoChunks(data, 2);
  console.log(new_data);
  let temp = "";
  let b = 0;

  for (let j = 0; j < new_data.length; j++) {
    temp += "<tr>";
    for (i = 0; i < new_data[j].length; i++) {
      temp +=
        ' <td class="osanpham"><img src="' +
        new_data[j][i].foodImageTest +
        '" /><p class="price">' +
        new_data[j][i].foodNameTest +
        "&emsp;&emsp;" +
        new_data[j][i].foodPriceTest +
        " VND" +
        "</p>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;" +
        `<button class="positive ui button" onclick="them_sp(${b})">Thêm vào giỏ</button><p>&emsp;</p></td>`;
      b++;
    }
    temp += "</tr>";
  }

  document.getElementById("themsp").innerHTML = temp;
}
//<input class="pricebutton" type="button" value="Đặt hàng" onclick="them_sp(${b})"/>
//<input type="button" value="Delete" class="nutbam" onclick="deleteItem(${i})" />
//Test hiển thị Cast
function display() {
  let minidata = "";
  let tongtien = 0;
  for (i = 0; i < data_cast.length; i++) {
    tongtien = tongtien + data_cast[i].foodPrice;
    minidata +=
      "<tr><td>" +
      data_cast[i].foodName +
      "</td><td>" +
      data_cast[i].foodPrice +
      `</td><td><button class="negative ui button" onclick="deleteItem(${i})">Xóa</button></td></tr>`;
  }
  minidata +=
    "<tr>" +
    '</td><td><div class="ui left action input"><button class="ui teal labeled icon button" onclick="ghitien()"><i class="cart icon"></i>Checkout</button><input type="text"' +
    ` value="${tongtien}" ></div></td></tr>`;
  document.getElementById("addd").innerHTML = minidata;
}
//Test thêm sp
function them_sp(x) {
  var data = localStorage.getItem("data");
  data = JSON.parse(data);
  data_cast.push(new FoodInfo(data[x].foodNameTest, data[x].foodPriceTest));
  display();
  console.log(data_cast);
}
//Test Xóa
function deleteItem(x) {
  data_cast.splice(x, 1);
  display();
}

let OrderInfo = function (user_name, user_email, food_name, food_price) {
  this.khachhang = user_name;
  this.khachhangemail = user_email;
  this.tenmon = food_name;
  this.giatien = food_price;
};
function ghitien() {
  var database_1 = localStorage.getItem("database_1");
  database_1 = JSON.parse(database_1);
  var vitri = localStorage.getItem("vitri");
  vitri = JSON.parse(vitri);
  for (i = 0; i < data_cast.length; i++) {
    data_order.push(
      new OrderInfo(
        database_1[vitri].userName,
        database_1[vitri].userEmail,
        data_cast[i].foodName,
        data_cast[i].foodPrice
      )
    );
  }
  localStorage.setItem("data_order", JSON.stringify(data_order));
  console.log(data_order);
  alert("Cám ơn quí khách đã sử dụng dịch vụ !!");
}
