const synth = window.speechSynthesis;

const textToSpeech = (string) => {
  let voice = new SpeechSynthesisUtterance(string);
  voice.text = string;
  voice.lang = "en-GB";
  voice.volume = 1;
  voice.rate = 1;
  voice.pitch = 1;
  synth.speak(voice);
};

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var recognition = new SpeechRecognition();
recognition.onresult = function (event) {
  var lastResult = event.results.length - 1;
  var content = event.results[lastResult][0].transcript;
  output(content);
};
function regStart() {
  recognition.start();
  let a = document.getElementById("robot");
  a.src = "/CaseStudy2/images/ironman2.gif";
  setTimeout(function () {
    a.src = "/CaseStudy2/images/123.gif";
  }, 3000);
}
