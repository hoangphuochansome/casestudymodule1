const userDatabase = [];
class UserData {
  constructor(name, email, password, birthday, gender) {
    this.userName = name;
    this.userEmail = email;
    this.userPassword = password;
    this.userBirthday = birthday;
    this.userGender = gender;
    this.user_Avatar = "../Source/user.png";
  }
}

//Hiển thị form đăng kí
function displayRegister() {
  document.getElementById(
    "monitor"
  ).innerHTML = ` <div id="login-form"><table border="0" class="form-dang-ki">
    <tr>
      <th colspan="2" style="padding-bottom: 50px">
        <h3 class="ui header">Đăng kí / Register</h3>
      </th>
    </tr>
    <tr>
      <td class="otieude">Tên / Name :</td>
      <td class="oinput">
        <div class="ui input">
          <input
            type="text"
            placeholder="Type your name ..."
            class="oinput"
            id="name"
          />
        </div>
        <div id="name-error"></div>
      </td>
    </tr>
    <tr>
      <td>Email :</td>
      <td>
        <div class="ui input">
          <input
            type="text"
            placeholder="Type your email ..."
            class="oinput"
            id="email"
          />
        </div>
        <div id="email-error"></div>
      </td>
    </tr>
    <tr>
      <td>Mật khẩu / Password :</td>
      <td>
        <div class="ui input">
          <input
            type="password"
            placeholder="Type your password ..."
            class="oinput"
            id="password"
          />
        </div>
        <div id="pass-error"></div>
      </td>
    </tr>
    <tr>
      <td>Ngày sinh / Birthday :</td>
      <td>
        <div class="ui input">
          <select id="ngay-sinh" class="ui dropdown">
            
          </select>
        </div>
        <div class="ui input">
          <select id="thang-sinh" class="ui dropdown">
            
          </select>
        </div>
        <div class="ui input">
          <select id="nam-sinh" class="ui dropdown">
            
          </select>
        </div>
      </td>
    </tr>
    <tr>
      <td>Giới tính / Gender :</td>
      <td>
        <input
          type="radio"
          value="nam"
          name="gender"
          checked
          id="gt-nam"
        />Nam&ensp; <input type="radio" value="nu" name="gender" />Nữ
      </td>
    </tr>
    <tr>
      <td></td>
      <td style="padding-top: 30px">
        <button class="ui primary button" onclick="getUserInfo()">
          Save
        </button>
        <button class="ui button" onclick="resetRegister()">Close</button>
      </td>
    </tr>
  </table></div>`;
  displayMonth();
  displayDay();
  displayYears();
}

// Hiển thị ngày tháng năm Automatic
function displayMonth() {
  let thang = "";
  for (i = 1; i <= 12; i++) {
    thang += '<option value="' + i + '">' + i + "</option>";
  }
  document.getElementById("thang-sinh").innerHTML = thang;
}

function displayDay() {
  let ngay = "";
  for (i = 1; i <= 30; i++) {
    ngay += '<option value="' + i + '">' + i + "</option>";
  }
  document.getElementById("ngay-sinh").innerHTML = ngay;
}

function displayYears() {
  let nam = "";
  for (i = 1970; i <= 2020; i++) {
    nam += '<option value="' + i + '">' + i + "</option>";
  }
  document.getElementById("nam-sinh").innerHTML = nam;
}

// Form Đăng Kí
function getUserInfo() {
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  let user_name = document.getElementById("name").value;
  let user_email = document.getElementById("email").value;
  let user_password = document.getElementById("password").value;
  let user_gender;
  let user_birthday =
    document.getElementById("ngay-sinh").value +
    "/" +
    document.getElementById("thang-sinh").value +
    "/" +
    document.getElementById("nam-sinh").value;
  if (document.getElementById("gt-nam").checked) {
    user_gender = "Nam";
  } else {
    user_gender = "Nữ";
  }
  if (user_name != "" && user_email != "" && user_password != "") {
    userDatabase.push(
      new UserData(
        user_name,
        user_email,
        user_password,
        user_birthday,
        user_gender
      )
    );
    document.getElementById("name-error").innerHTML = "";
    document.getElementById("email-error").innerHTML = "";
    document.getElementById("pass-error").innerHTML = "";
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
    alert("Chúc mừng bạn đã đăng kí thành công !");
  } else {
    if (user_name == "") {
      document.getElementById("name-error").innerHTML =
        "Điền tên của bạn / Input your name !";
    } else {
      document.getElementById("name-error").innerHTML = "";
    }
    if (user_email == "") {
      document.getElementById("email-error").innerHTML =
        "Điền tên của bạn / Input your name !";
    } else {
      document.getElementById("email-error").innerHTML = "";
    }
    if (user_password == "") {
      document.getElementById("pass-error").innerHTML =
        "Điền tên của bạn / Input your name !";
    } else {
      document.getElementById("pass-error").innerHTML = "";
    }
  }
  console.log(userDatabase);
  localStorage.setItem("userDatabase", JSON.stringify(userDatabase));
}
function resetRegister() {
  document.getElementById("monitor").innerHTML = "";
}

//Hiển thị form đăng nhập
function displayLoginForm() {
  document.getElementById("monitor").innerHTML = `<div id="login-form">
    <table border="0" style="margin-left: 115px; margin-top: 50px">
      <tr>
        <td colspan="2">
          <img src="../Source/logo.png" style="width: 100px; float: left" />
          <h2 class="ui header" style="padding-top: 20px">
            &emsp;&emsp;Log-in to your account
          </h2>
        </td>
      </tr>
      <tr>
        <td style="width: 150px">Email</td>
        <td>
          <div class="field">
            <div class="ui left icon input">
              <i class="user icon"></i>
              <input
                type="text"
                name="email"
                placeholder="E-mail address"
                style="width: 290px"
                id="email-login"
              />
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>Password</td>
        <td>
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i>
              <input
                type="password"
                name="password"
                placeholder="Password"
                style="width: 290px"
                id="pass-login"
              />
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td></td>
        <td style="padding-top: 25px">
          <button class="ui secondary button" onclick = "login()">Login</button>
          <button class="ui button" onclick = "resetRegister()">Cancel</button>
        </td>
      </tr>
    </table>
  </div>`;
}
function login() {
  let currentUser = "";
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  console.log(userDatabase);
  let user_login_email = document.getElementById("email-login").value;
  let user_login_pass = document.getElementById("pass-login").value;
  let check = false;
  for (i = 0; i < userDatabase.length; i++) {
    if (
      user_login_email == userDatabase[i].userEmail &&
      user_login_pass == userDatabase[i].userPassword
    ) {
      currentUser = userDatabase[i].userName;
      setTimeout(function () {
        window.location.href = "../html/homepage.html";
      }, 1000);
      check = true;
      break;
    } else if (
      user_login_email == userDatabase[i].userEmail &&
      user_login_pass != userDatabase[i].userPassword
    ) {
      alert("Wrong Password");
      check = true;
      break;
    }
  }
  if (check == false) {
    confirm("Chua dang ki");
  }

  localStorage.setItem("currentUser", JSON.stringify(currentUser));
}
//HOME PAGE
const HomePageData = [];
var currentUser = localStorage.getItem("currentUser");
currentUser = JSON.parse(currentUser);
class NewFeed {
  constructor(username, user_image, user_post) {
    this.userName = username;
    this.userAvatar = user_image;
    this.userPost = user_post;
    this.count_like = 0;
    this.count_comment = 0;
    this.typping = "";
    this.Comment = [];
    this.currentUser = currentUser;
  }
  addLike() {
    this.count_like += 1;
    displayHomePage();
  }
  addCount_Comment(x) {
    this.count_comment += x;
  }
  showComment() {
    this.typping = "";
    for (let i = 0; i < this.Comment.length; i++) {
      this.typping +=
        "&emsp;" + this.currentUser + " :&emsp;" + this.Comment[i] + "<br>";
    }
    console.log(this.typping);
    return this.typping;
  }
  addComment(text) {
    this.Comment.push(text);
    this.count_comment++;
    displayHomePage();
  }
}
//localStorage.removeItem("HomePageData");
//localStorage.setItem("HomePageData", JSON.stringify(HomePageData));
//Hiển thị New Feed
function displayHomePage() {
  var HomePageData = localStorage.getItem("HomePageData");
  HomePageData = JSON.parse(HomePageData);
  let temp1 = "";
  console.log(HomePageData);
  for (let i = HomePageData.length - 1; i >= 0; i--) {
    temp1 +=
      `<div
    class="ui card"
    style="
      width: 600px;
      min-height: 500px;
      margin-bottom: 20px;
      margin-top: 20px;
    "
  >
    <div class="content">
     
      <img
        class="ui avatar image"
        src="` +
      HomePageData[i].userAvatar +
      `"
      />
      ` +
      HomePageData[i].userName +
      `
    </div>
    <div class="image">
      <img src="` +
      HomePageData[i].userPost +
      `" />
    </div>
    <div class="content">
      <span class="right floated">
        <i class="heart outline like icon" onclick="do_add_like( ${i} )"></i>
        ` +
      `<span id="countlike_${i}">` +
      HomePageData[i].count_like +
      `</span>` +
      ` likes
      </span>
      <i class="comment icon"></i>
      ` +
      `<span id="countcomment_${i}">` +
      HomePageData[i].count_comment +
      `</span>` +
      ` comments
    </div>
    <div id="show${i}" style="padding-bottom: 20px"></div>
    <div class="extra content">
      <div class="ui large transparent left icon input">
        <i class="heart outline icon"></i>
        <input id="commentinput${i}" type="text" placeholder="Add Comment..." onclick="midDetect(${i})"/>
      </div>
    </div>
  </div>`;
  }
  document.getElementById("main").innerHTML = temp1;
}
//Add like
function do_add_like(i) {
  var HomePageData = localStorage.getItem("HomePageData");
  HomePageData = JSON.parse(HomePageData);
  HomePageData[i].count_like += 1;
  console.log(HomePageData[i]);
  document.getElementById(`countlike_${i}`).innerHTML =
    HomePageData[i].count_like;
  localStorage.setItem("HomePageData", JSON.stringify(HomePageData));
}

// Trung gian nhận Input Comment
function midDetect(x) {
  var HomePageData = localStorage.getItem("HomePageData");
  HomePageData = JSON.parse(HomePageData);
  var currentUser = localStorage.getItem("currentUser");
  currentUser = JSON.parse(currentUser);
  show_Comment(x);
  let inputField = document.getElementById(`commentinput${x}`);
  inputField.addEventListener("keydown", function (e) {
    if (e.code == "Enter") {
      HomePageData[x].Comment.push(
        "&emsp;" + currentUser + " : " + inputField.value
      );
      //console.log(HomePageData[x].Comment);
      HomePageData[x].count_comment += 1;
      document.getElementById(`countcomment_${x}`).innerHTML =
        HomePageData[x].count_comment;
      localStorage.setItem("HomePageData", JSON.stringify(HomePageData));
      show_Comment(x);
      inputField.value = "";
    }
  });
}
function show_Comment(x) {
  var HomePageData = localStorage.getItem("HomePageData");
  HomePageData = JSON.parse(HomePageData);
  console.log(HomePageData[x].Comment);
  temp = "";
  for (i = 0; i < HomePageData[x].Comment.length; i++) {
    temp += HomePageData[x].Comment[i] + "<br>";
  }
  document.getElementById(`show${x}`).innerHTML = temp;
}
//Show Profile Menu
function profileMenu() {
  document.getElementById(
    "menu-s"
  ).innerHTML = `<div id="personal"><table id="personal-table">
    <tr>
      <td><a href="../html/profile.html">Profile</a></td>
    </tr>
    <tr>
    <td><a href="../html/homepage.html">Home</a></td>
    </tr>
    <tr>
    <td><a href="../html/landing-page.html">Log out</a></td>
    </tr>
  </table></div>`;
  setTimeout(function () {
    document.getElementById("menu-s").innerHTML = "";
  }, 2000);
}
// Information User
function showUserInfo() {
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  var currentUser = localStorage.getItem("currentUser");
  currentUser = JSON.parse(currentUser);
  console.log(userDatabase[0].user_Avatar);
  console.log(currentUser);
  let temp = "";
  for (let i = 0; i < userDatabase.length; i++) {
    if (userDatabase[i].userName == currentUser) {
      temp =
        `<div class="wrapper">
          <div class="left">
            <img src="` +
        userDatabase[i].user_Avatar +
        `" alt="user" width="100" />
            <h4>` +
        userDatabase[i].userName +
        `</h4>
            <p>User</p>
          </div>
          <div class="right">
            <div class="info">
              <h3>Information</h3>
              <div class="info_data">
                <div class="data">
                  <h4>Email</h4>
                  <p>` +
        userDatabase[i].userEmail +
        `</p>
                </div>
                <div class="data">
                  <h4>Password</h4>
                  <p>` +
        userDatabase[i].userPassword +
        `</p>
                </div>
              </div>
            </div>
  
            <div class="projects">
              <h3>Timeline</h3>
              <div class="projects_data">
                <div class="data">
                  <h4>Add new image</h4>
                  <div class="ui labeled input">
  <div class="ui label">
    ../
  </div>
  <input id="linkImage" type="text" placeholder="Input image's link ...">
</div>
                </div>
                
              </div>
            </div>
  
            <div class="social_media" style="margin-top: 150px">
              <button class="ui primary button" onclick="addNewImage()" id="save-button">Save</button>
              <button class="ui button" onclick="displayEditUser()">Edit</button>
            </div>
          </div>
        </div>`;
    }
  }
  document.getElementById("main").innerHTML = temp;
}

//Add new Image
function addNewImage() {
  var HomePageData = localStorage.getItem("HomePageData");
  HomePageData = JSON.parse(HomePageData);
  var currentUser = localStorage.getItem("currentUser");
  currentUser = JSON.parse(currentUser);
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  for (let i = 0; i < userDatabase.length; i++) {
    if (userDatabase[i].userName == currentUser) {
      HomePageData.push(
        new NewFeed(
          currentUser,
          userDatabase[i].user_Avatar,
          document.getElementById("linkImage").value
        )
      );
    }
  }
  alert("Upload Complete");
  localStorage.setItem("HomePageData", JSON.stringify(HomePageData));
  console.log(HomePageData);
}

//
// Edit Info
function displayEditUser() {
  document.getElementById("main").innerHTML = "";
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  var currentUser = localStorage.getItem("currentUser");
  currentUser = JSON.parse(currentUser);

  console.log(currentUser);
  let temp = "";
  for (let i = 0; i < userDatabase.length; i++) {
    if (userDatabase[i].userName == currentUser) {
      console.log(userDatabase[i].user_Avatar);

      temp =
        `<div class="wrapper">
          <div class="left">
            <img id="user_avatar" src="` +
        userDatabase[i].user_Avatar +
        `" alt="user" width="100" /><div class="ui input focus">
        <input type="text" placeholder="Avatar link..." id="edit_avatar">
      </div>
      <div class="ui input focus">
      <input id="edit_username" type="text" placeholder="Search..." value="` +
        userDatabase[i].userName +
        `">
    </div>     
           <p>User</p>
          </div>
          <div class="right">
            <div class="info">
              <h3>Information</h3>
              <div class="info_data">
                <div class="data">
                  <h4>Email</h4>
                  <div class="ui input focus">
      <input id="edit_email" type="text" placeholder="Search..." value="` +
        userDatabase[i].userEmail +
        `">
    </div>
                </div>
                <div class="data">
                  <h4>Password</h4>
                  <div class="ui input focus">
      <input id="edit_password" type="text" placeholder="Search..." value="` +
        userDatabase[i].userPassword +
        `">
    </div>
                </div>
              </div>
            </div>
  
            <div class="projects">
              <h3>Timeline</h3>
              <div class="projects_data">
                <div class="data">
                  <h4>Add new image</h4>
                  <div class="ui labeled input">
  <div class="ui label">
    ../
  </div>
  <input id="linkImage" type="text" placeholder="Input image's link ...">
</div>
                </div>
                
              </div>
            </div>
  
            <div class="social_media" style="margin-top: 150px">
              <button class="ui primary button" onclick="editUserInfo()" id="save-button">Save</button>
              <button class="ui button" onclick="showUserInfo()">Cancel</button>
            </div>
          </div>
        </div>`;
    }
  }
  document.getElementById("main").innerHTML = temp;
}
function editUserInfo() {
  let new_userAvatar = document.getElementById("edit_avatar").value;
  let new_userName = document.getElementById("edit_username").value;
  let new_userEmail = document.getElementById("edit_email").value;
  let new_userPassword = document.getElementById("edit_password").value;
  let userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  var currentUser = localStorage.getItem("currentUser");
  currentUser = JSON.parse(currentUser);
  for (let i = 0; i < userDatabase.length; i++) {
    if (userDatabase[i].userName == currentUser) {
      currentUser = new_userName;
      userDatabase[i].user_Avatar = new_userAvatar;
      userDatabase[i].userName = new_userName;
      userDatabase[i].userPassword = new_userPassword;
      userDatabase[i].userEmail = new_userEmail;
    }
  }
  console.log(new_userAvatar);
  document.getElementById("user_avatar").src = `${new_userAvatar}`;
  console.log(userDatabase);
  localStorage.setItem("userDatabase", JSON.stringify(userDatabase));
  localStorage.setItem("currentUser", JSON.stringify(currentUser));
  showUserInfo();
}

let OTP = 0;
let checkEmail = "";
//Hiển thị form quên mật khẩu
function displayForgot() {
  document.getElementById("monitor").innerHTML = "";
  document.getElementById("monitor").innerHTML = `<div id="forgot-password">
    <table border="0" style="margin-left: 50px">
      <tr>
        <td colspan="2">
          <img src="../Source/logo.png" width="100px" />
          <h3
            class="ui header"
            style="
              margin-top: -60px;
              margin-left: 90px;
              padding-bottom: 40px;
            "
          >
            &emsp;&emsp;Forgot Password
          </h3>
        </td>
      </tr>
      <tr>
        <td style="padding-right:40px">Email</td>
        <td>
          <div class="ui left icon input">
            <input type="text" placeholder="Input your email..." id="forgot-email"/>
            <i class="users icon"></i>
          </div>
        </td>
      </tr>
      <tr>
        <td>OTP Code</td>
        <td>
          <div class="ui icon input loading">
            <input type="text" placeholder="Input OTP code..." id="otp_code_input"/>
            <i class="search icon"></i>
          </div>
        </td>
      </tr>
      <tr>
        <td></td>
        <td style="padding-top:10px">
          <button class="ui secondary button" onclick="checkOTP()">Confirm</button>
          <button class="ui button" onclick="deleteForgot()">Cancel</button>
        </td>
      </tr>
    </table>
  </div>`;
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  const inputEmail = document.getElementById("forgot-email");
  inputEmail.addEventListener("keydown", function (e) {
    if (e.code === "Enter") {
      let input = inputEmail.value;
      for (let i = 0; i < userDatabase.length; i++) {
        if (input == userDatabase[i].userEmail) {
          OTP = otpcreate();
          checkEmail = input;
          console.log(checkEmail);
          break;
        }
      }
    }
  });
}
function deleteForgot() {
  document.getElementById("monitor").innerHTML = "";
}

//Tạo OTP
function otpcreate() {
  let otp = Math.round(Math.random() * (9999 - 1000) + 1000);
  setTimeout(function () {
    alert("Phuoc's Team : Your OTP code is : " + otp);
  }, 1500);
  return otp;
}
//Check OTP
function checkOTP() {
  let otp = document.getElementById("otp_code_input").value;
  if (otp == OTP) {
    displayNewPassword();
  }
}
// Tạo form lấy mật khẩu mới
function displayNewPassword() {
  document.getElementById("monitor").innerHTML = `<div id="set_new_password">
    <table border="0" style="margin-left: 38px; margin-top: 2px">
      <tr>
        <td colspan="2">
          <img src="../Source/logo.png" width="100px" />
          <h3
            class="ui header"
            style="
              margin-top: -60px;
              margin-left: 90px;
              padding-bottom: 40px;
            "
          >
            &emsp;&emsp;New Password
          </h3>
        </td>
      </tr>
      <tr>
        <td style="width: 120px; text-align: left">Mật khẩu mới</td>
        <td>
          <div class="ui input focus">
            <input type="password" placeholder="New Password..." id="mk-1" />
          </div>
        </td>
      </tr>
      <tr>
        <td style="text-align: left">Nhập lại</td>
        <td>
          <div class="ui input focus">
            <input
              type="password"
              placeholder="Confirm Password..."
              id="mk-2"
            />
          </div>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <button class="ui right labeled icon button" onclick="checkPassword()">
            <i class="right arrow icon"></i>
            Confirm
          </button>
        </td>
      </tr>
    </table>
  </div>`;
}
//Check email
function checkPassword() {
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);

  let mk1 = document.getElementById("mk-1").value;
  let mk2 = document.getElementById("mk-2").value;
  if (mk1 == mk2) {
    for (i = 0; i < userDatabase.length; i++) {
      if (checkEmail == userDatabase[i].userEmail) {
        userDatabase[i].userPassword = mk1;
        console.log(userDatabase);
        localStorage.setItem("userDatabase", JSON.stringify(userDatabase));
        alert("Đổi mật khẩu mới thành công !");
        displayLoginForm();
        break;
      }
    }
  } else {
    alert("Mật khẩu không trùng khớp");
  }
}
//Hiển thị bảng người dùng đã đăng kí
function display_register_user() {
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  console.log(userDatabase);
  document.getElementById("monitor-s").innerHTML = "";
  let temp = `<div id="monitor">
    <table
      border="0"
      style="margin-left: 10px; margin-top: 25px; border-collapse: separate"
    >
      <tr>
        <td class="otieude">Tên</td>
        <td class="otieude">Email</td>
        <td class="otieude">Mật khẩu</td>
        <td class="otieude">Ngày sinh</td>
        <td class="otieude">Giới tính</td>
        <td style="padding-bottom: 20px; font-size:16px">Action</td>
      </tr>`;
  for (let i = 0; i < userDatabase.length; i++) {
    temp +=
      "<tr><td>" +
      userDatabase[i].userName +
      "</td><td>" +
      userDatabase[i].userEmail +
      "</td><td>" +
      userDatabase[i].userPassword +
      "</td><td>" +
      userDatabase[i].userBirthday +
      "</td><td>" +
      userDatabase[i].userGender +
      `</td><td><button class="ui red button" onclick="delete_User(${i})">Delete</button></td></tr>`;
  }
  temp += "</table></div>";
  document.getElementById("monitor-s").innerHTML = temp;
}
// Xóa user
function delete_User(i) {
  var userDatabase = localStorage.getItem("userDatabase");
  userDatabase = JSON.parse(userDatabase);
  userDatabase.splice(i, 1);
  localStorage.setItem("userDatabase", JSON.stringify(userDatabase));
  display_register_user();

  setTimeout(function () {
    document.getElementById("monitor-s").innerHTML = "";
  }, 4000);
}
